﻿Feature: IndexPageTest
I want to be able load the index page

@smoke
Scenario: Entering http address should open the index page
	Given I open Swag Labs web site
	When I launch Index page
	Then The page with 'Swag Labs' title should be opened
