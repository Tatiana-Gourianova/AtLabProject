﻿using ATLabProject.Base;

namespace ATLabProject.Pages
{
    public class IndexPage : BasePage
    {
        private string URL => "https://www.saucedemo.com/";
        private string DocTitle = "Swag Labs";

        private static IndexPage? indexPage;
        public static IndexPage Instance => indexPage ?? (indexPage = new IndexPage());
        public void OpenIndexPage()
        {
            DriverManager.Instance().Navigate().GoToUrl(URL);
        }


        public bool IsPageTitleDisplayed(string item)
        {
            return DocTitle.Equals(item);
        }
    }
}
