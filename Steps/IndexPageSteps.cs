﻿using ATLabProject.Base;
using ATLabProject.Pages;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace ATLabProject.Steps
{
    [Binding]
    public sealed class IndexPageSteps
    {

        private readonly ScenarioContext context;
        public IndexPageSteps(ScenarioContext injectedContext)
        {
            context = injectedContext;
        }
        [Given(@"I open Swag Labs web site")]
        public void OpenSwagLabsWebSite()
        {
            IndexPage.Instance.OpenIndexPage();
        }
        [When(@"I launch Index page")]
        public void IndexPageOpen()
        {
            try
            {
                IndexPage.Instance.OpenIndexPage();
            }
            catch
            { throw new PendingStepException(); }
        }

        [Then(@"The page with '([^']*)' title should be opened")]
        public void PageWithTitleShouldBeOpened(string title)
        {

            try { Assert.IsTrue(IndexPage.Instance.IsPageTitleDisplayed("Swag Labs"), "Page title for this page is not found"); }
            catch
            {
                throw new PendingStepException();
            }
        }

        [AfterTestRun]
        public static void AfterTestRun()
        {
            DriverManager.QuitDriver();
        }
    }
}